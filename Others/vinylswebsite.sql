-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 16 avr. 2019 à 16:20
-- Version du serveur :  5.7.17
-- Version de PHP :  7.1.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `vinylswebsite`
--
CREATE DATABASE IF NOT EXISTS `vinylswebsite` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `vinylswebsite`;

-- --------------------------------------------------------

--
-- Structure de la table `tracks`
--
-- Création :  lun. 08 avr. 2019 à 13:59
-- Dernière modification :  jeu. 11 avr. 2019 à 08:54
--

CREATE TABLE `tracks` (
  `idTrack` int(11) NOT NULL,
  `title` text NOT NULL,
  `artist` text NOT NULL,
  `style` text NOT NULL,
  `duration` int(100) NOT NULL,
  `bpm` int(5) NOT NULL,
  `notationTrack` int(5) NOT NULL,
  `commentTrack` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tracksvinyls`
--
-- Création :  jeu. 07 fév. 2019 à 09:56
--

CREATE TABLE `tracksvinyls` (
  `idTrack` int(11) NOT NULL,
  `idVinyl` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--
-- Création :  jeu. 07 fév. 2019 à 09:42
--

CREATE TABLE `users` (
  `idUser` int(11) NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `username` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `usersvinyls`
--
-- Création :  jeu. 07 fév. 2019 à 09:49
--

CREATE TABLE `usersvinyls` (
  `idUser` int(11) NOT NULL,
  `idVinyl` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `vinyls`
--
-- Création :  jeu. 21 mars 2019 à 10:07
--

CREATE TABLE `vinyls` (
  `idVinyl` int(11) NOT NULL,
  `nameVinyl` text NOT NULL,
  `artist` text NOT NULL,
  `dateCreation` date NOT NULL,
  `label` text NOT NULL,
  `format` text NOT NULL,
  `genre` text NOT NULL,
  `style` text NOT NULL,
  `coverImage` text NOT NULL,
  `edition` text NOT NULL,
  `country` text NOT NULL,
  `personnalNote` text NOT NULL,
  `notationVinyl` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `tracks`
--
ALTER TABLE `tracks`
  ADD PRIMARY KEY (`idTrack`);

--
-- Index pour la table `tracksvinyls`
--
ALTER TABLE `tracksvinyls`
  ADD KEY `idVinyl` (`idVinyl`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`);

--
-- Index pour la table `usersvinyls`
--
ALTER TABLE `usersvinyls`
  ADD KEY `idUser` (`idUser`),
  ADD KEY `idVinyl` (`idVinyl`);

--
-- Index pour la table `vinyls`
--
ALTER TABLE `vinyls`
  ADD PRIMARY KEY (`idVinyl`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `tracks`
--
ALTER TABLE `tracks`
  MODIFY `idTrack` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `vinyls`
--
ALTER TABLE `vinyls`
  MODIFY `idVinyl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `tracksvinyls`
--
ALTER TABLE `tracksvinyls`
  ADD CONSTRAINT `tracksvinyls_ibfk_1` FOREIGN KEY (`idVinyl`) REFERENCES `vinyls` (`idVinyl`);

--
-- Contraintes pour la table `usersvinyls`
--
ALTER TABLE `usersvinyls`
  ADD CONSTRAINT `usersvinyls_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `users` (`idUser`),
  ADD CONSTRAINT `usersvinyls_ibfk_2` FOREIGN KEY (`idVinyl`) REFERENCES `vinyls` (`idVinyl`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
