<?php

/* * ****************************************

 * ** Projet : Examen Blanc M151

 * ** Description : Site web pour répértorier des films et les noter

 * **

 * ** Date : 30.08.2018

 * ** Auteur : Loic MGN

 * ***************************************** */


include '../Model/connectionModel.php';

if (isset($_POST ['submit'])) {
    $error = array();
    $email = trim(filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING));
    $pass = trim(filter_input(INPUT_POST, 'pwd', FILTER_SANITIZE_STRING));
    $users = getUserLogin($email, $pass);
    // Vérifie si il y a quelque chose de retourner. Sinon ajoute une erreur à la case email dans le tableau erreurs
    if (empty($pass)) {
        $error ['Password'] = 'Veuillez entrer un mot de passe valide !';
    }
    if (empty($email)) {
        $error ['pseudoInpt'] = 'Veuillez entrer un pseudo valide !';
    } else {
        // Si il n'est pas vide il vas vérifiez que le mot de passe et l'email sont correspondant. Sinon ajoute une erreur		
        if ($email != $users[0]['email'] || $pass != $users[0]['password']) {
            $error ['Email'] = 'Le pseudo ou le mot de passe est faux !';
        }
    }
    if (empty($error)) {
        session_start();
            $_SESSION ['connected'] = true;
            $_SESSION ['email'] = $email;
            $_SESSION ['user'] = $users[0]['username'];
            $_SESSION ['userId'] = $users[0]['idUser'];
            
            include '../View/vinyls.php';      
    }
    else{
        echo $error;
        include '..View/index.php';
    }
}
