<?php

session_start();

require "../Model/addTrackModel.php";
//require "addVinylController.php";

$trackTitles = filter_input(INPUT_POST, 'trackTitle', FILTER_SANITIZE_STRING);
$trackArtist = filter_input(INPUT_POST, 'trackArtist', FILTER_SANITIZE_STRING);
$trackStyle = filter_input(INPUT_POST, 'trackStyle', FILTER_SANITIZE_STRING);
$trackDuration = filter_input(INPUT_POST, 'trackDuration', FILTER_SANITIZE_NUMBER_INT);
$trackBpm = filter_input(INPUT_POST, 'trackBpm', FILTER_SANITIZE_NUMBER_INT);
$trackNotation = filter_input(INPUT_POST, 'trackNotation', FILTER_SANITIZE_NUMBER_INT);
$trackComment = filter_input(INPUT_POST, 'trackComment', FILTER_SANITIZE_STRING);

$idVinyl = $_SESSION["idLastVinylInserted"];

for ($i = 0; $i < (count($trackTitles)-1); $i++) {
    
    $idTrack = addTrack($trackTitles[$i], $trackArtist[$i], $trackStyle[$i], $trackDuration[$i], $trackBpm[$i], $trackNotation[$i], $trackComment[$i]);
    addConnectionTrack($idTrack, $idVinyl);
}

include '../View/vinyls.php';