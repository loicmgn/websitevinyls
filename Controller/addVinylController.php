<?php
session_start();
include '../Model/addVinylModel.php';

if (isset($_POST['submitNewVinyl']))
{
    $nameVinyl = filter_input(INPUT_POST, 'nameVinyl', FILTER_SANITIZE_STRING);
    $artistVinyl = filter_input(INPUT_POST, 'artistVinyl', FILTER_SANITIZE_STRING);
    $dateVinyl = filter_input(INPUT_POST, 'dateVinyl', FILTER_SANITIZE_STRING);
    $labelVinyl = filter_input(INPUT_POST, 'labelVinyl', FILTER_SANITIZE_STRING);
    $genreVinyl = filter_input(INPUT_POST, 'listGenreVinyl', FILTER_SANITIZE_STRING);
    $styleVinyl = filter_input(INPUT_POST, 'styleVinyl', FILTER_SANITIZE_STRING);
    $formatVinyl = filter_input(INPUT_POST, 'formatVinyl', FILTER_SANITIZE_STRING);
    $editionVinyl = filter_input(INPUT_POST, 'editionVinyl', FILTER_SANITIZE_STRING);
    $countryVinyl = filter_input(INPUT_POST, 'countryVinyl', FILTER_SANITIZE_STRING);
    $persoNoteVinyl = filter_input(INPUT_POST, 'persoNoteVinyl', FILTER_SANITIZE_STRING);
    $ratingVinyl = filter_input(INPUT_POST, 'ratingStar', FILTER_SANITIZE_STRING);
    $file = $_FILES["imageVinyl"];
   
    $nameMedia = $file['name'];
    $target_file = $file['tmp_name'];
    $target_dir = "../medias/" . basename($nameMedia);
    $uploadOk = 1;

    $check = getimagesize($target_file);
    if ($check !== false) { 
        $userId = $_SESSION['userId'];
        $uploadOk = 1;
        move_uploaded_file($target_file, $target_dir);
        $idVinyl = addVinyl($nameVinyl, $artistVinyl, $dateVinyl, $labelVinyl, $genreVinyl, $styleVinyl, $formatVinyl, $editionVinyl, $countryVinyl, $nameMedia, $persoNoteVinyl, $ratingVinyl);
        $_SESSION["idLastVinylInserted"] = $idVinyl;
        addConnectionToUser($userId, $idVinyl);     
    } else {
        echo "File is not an image.";
        $uploadOk = 0;                        
    }
    include '../View/addTracks.php';
}
else{
    include '../View/addVinyl.php';
}

