<?php
include '../Model/registerModel.php';

if (isset($_POST ['submitRegister'])) {

    $error = array();
    $email = trim(filter_input(INPUT_POST, 'emailRegister', FILTER_SANITIZE_STRING));
    $password = trim(filter_input(INPUT_POST, 'pwdRegister', FILTER_SANITIZE_STRING));
    $passwordConf = trim(filter_input(INPUT_POST, 'pwdConfRegister', FILTER_SANITIZE_STRING));
    $username = trim(filter_input(INPUT_POST, 'usernameRegister', FILTER_SANITIZE_STRING));
    if (empty($email)) {
        $error ['email'] = 'Veuillez fournir une adresse e-mail !';
    }
    if (empty($password)) {
        $error ['password'] = 'Veuillez fournir un mot de passe !';
    }
    if (empty($passwordConf)) {
        $error ['passwordConf'] = 'Veuillez fournir une confirmation de mot de passe !';
    }
    if (empty($username)) {
        $error ['username'] = 'Veuillez fournir une nom d\'utilisateur !';
    } 
    if ($password != $passwordConf) {
        $error['passConf'] = "Les mots de passes ne sont pas identiques";
    }

    if (empty($error)) {

        register($email, $username, $password);
            //die();
        header('Location: ../View/index.php');

    }
}

