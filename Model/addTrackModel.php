<?php
require '../Model/dbConnection.php';

function addTrack($trackTitle, $trackArtist, $trackStyle, $trackDuration, $trackBpm, $trackNotation, $trackComment) {
    try {
        $connect = myDatabase();
        $req = $connect->prepare('INSERT INTO `tracks`(`title`, `artist`,`style`, `duration`, `bpm`, `notationTrack`, `commentTrack`) VALUES (:trackTitle, :trackArtist, :trackStyle, :trackDuration, :trackBpm, :trackNotation, :trackComment)');
        $req->bindParam(':trackTitle', $trackTitle, PDO::PARAM_STR);
        $req->bindParam(':trackArtist', $trackArtist, PDO::PARAM_STR);
        $req->bindParam(':trackStyle', $trackStyle, PDO::PARAM_STR);
        $req->bindParam(':trackDuration', $trackDuration, PDO::PARAM_INT);
        $req->bindParam(':trackBpm', $trackBpm, PDO::PARAM_INT);
        $req->bindParam(':trackNotation', $trackNotation, PDO::PARAM_INT);
        $req->bindParam(':trackComment', $trackComment, PDO::PARAM_STR);
        $req->execute();
        RETURN $connect->lastInsertId();
    } catch (Exception $ex) {
        return $ex;
    }
}

function addConnectionTrack($idTrack, $idVinyl)
{
    try {
        $connect = myDatabase();
        $req = $connect->prepare('INSERT INTO `tracksvinyls`(`idTrack`,`idVinyl`) VALUES (:idTrack, :idVinyl)');
        $req->bindParam(':idTrack', $idTrack, PDO::PARAM_STR);
        $req->bindParam(':idVinyl', $idVinyl, PDO::PARAM_STR);
        $req->execute();
        RETURN TRUE;
    } catch (Exception $ex) {
        return $ex;
    }
}