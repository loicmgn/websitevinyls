<?php

require '../Model/dbConnection.php';

function addVinyl($nameVinyl, $artistVinyl, $dateVinyl, $labelVinyl, $genreVinyl, $styleVinyl, $formatVinyl, $editionVinyl, $countryVinyl, $imageVinyl, $persoNoteVinyl, $ratingVinyl) {
    try {
        $connect = myDatabase();
        $req = $connect->prepare('INSERT INTO `vinyls`(`nameVinyl`, `artist`,`dateCreation`, `label`, `format`, `genre`, `style`, `coverImage`,`edition`, `country`, `personnalNote`, `notationVinyl` ) VALUES (:nameVinyl, :artist, :dateCreation, :label, :format, :genre, :style, :coverImage, :edition, :country, :personnalNote, :notationVinyl)');
        $req->bindParam(':nameVinyl', $nameVinyl, PDO::PARAM_STR);
        $req->bindParam(':artist', $artistVinyl, PDO::PARAM_STR);
        $req->bindParam(':dateCreation', $dateVinyl, PDO::PARAM_STR);
        $req->bindParam(':label', $labelVinyl, PDO::PARAM_STR);
        $req->bindParam(':genre', $genreVinyl, PDO::PARAM_STR);
        $req->bindParam(':style', $styleVinyl, PDO::PARAM_STR);
        $req->bindParam(':format', $formatVinyl, PDO::PARAM_STR);
        $req->bindParam(':edition', $editionVinyl, PDO::PARAM_STR);
        $req->bindParam(':country', $countryVinyl, PDO::PARAM_STR);
        $req->bindParam(':coverImage', $imageVinyl, PDO::PARAM_STR);
        $req->bindParam(':personnalNote', $persoNoteVinyl, PDO::PARAM_STR);
        $req->bindParam(':notationVinyl', $ratingVinyl, PDO::PARAM_STR);
        $req->execute();
        RETURN $connect->lastInsertId();
    } catch (Exception $ex) {
        return $ex;
    }
}

function addConnectionToUser($idUser, $idVinyl)
{
    try {
        $connect = myDatabase();
        $req = $connect->prepare('INSERT INTO `usersvinyls`(`idUser`,`idVinyl`) VALUES (:idUser, :idVinyl)');
        $req->bindParam(':idUser', $idUser, PDO::PARAM_STR);
        $req->bindParam(':idVinyl', $idVinyl, PDO::PARAM_STR);
        $req->execute();
        RETURN TRUE;
    } catch (Exception $ex) {
        return $ex;
    }
}
