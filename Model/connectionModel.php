<?php

require 'dbConnection.php';

function getUserLogin($email, $password) {
    try {
        $connect = myDatabase();
        $req = $connect->prepare("SELECT * FROM users WHERE email = :email AND password = :password");
        $req->bindParam(':email', $email, PDO::PARAM_STR);
        $req->bindParam(':password', $password, PDO::PARAM_STR);
        $req->execute();
        $result = $req->fetchAll($fetch_style = PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return FALSE;
    }
    //Utile pour le if de la page index.php
    if ($result != NULL) {
        return $result;
    } else {
        return FALSE;
    }
}
