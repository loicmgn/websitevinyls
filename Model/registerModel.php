<?php
require '../Model/dbConnection.php';

function register($email, $username, $password) {
    try {
        $connect = myDatabase();
        $req = $connect->prepare('INSERT INTO `users`(`email`, `password`,`username` ) VALUES (:email, :password, :username)');
        $req->bindParam(':email', $email, PDO::PARAM_STR);
        $req->bindParam(':password', $password, PDO::PARAM_STR);
        $req->bindParam(':username', $username, PDO::PARAM_STR);
        $req->execute();
        RETURN TRUE;
    } catch (Exception $ex) {
        return FALSE;
    }
}