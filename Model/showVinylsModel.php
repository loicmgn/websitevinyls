<?php

require_once 'dbConnection.php';

function getVinyls($idUser){
    try {
        $connect = myDatabase();
        $req = $connect->prepare("SELECT * FROM vinyls JOIN usersvinyls WHERE usersvinyls.idVinyl = vinyls.idVinyl AND usersvinyls.idUser = :idUser");
        $req->bindParam(':idUser', $idUser, PDO::PARAM_STR);
        $req->execute();
        $result = $req->fetchAll($fetch_style = PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return FALSE;
    }
    //Utile pour le if de la page index.php
    if ($result != NULL) {
        return $result;
    } else {
        return FALSE;
    }
}