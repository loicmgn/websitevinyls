<!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/4-col-portfolio.css" rel="stylesheet">

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="../View/vinyls.php">Vinyzi</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Login
              <span class="sr-only">(current)</span>
              </a>          
            </li>
            <li class="nav-item">
              <a class="nav-link" href="register.php">Register</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="container" id="divLogin">
        <div class="row">
            <div class="col-sm">
                
            </div>
            <div class="col-sm" id="colTitle">
                <h3>Login</h3>
            </div>
            <div class="col-sm">

            </div>
        </div>
        <form action="../Controller/connectionController.php" method="POST">       
        <div class="form-group">
            <h5><label for="email">Email address :</label></h5>
            <input type="email" class="form-control" id="email" name="email">
        </div>
        <div class="form-group">
            <h5><label for="pwd">Password :</label></h5>
            <input type="password" class="form-control" id="pwd" name="pwd">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <div>
            <h6>No account yet ? Create one <a href="../View/register.php">HERE</a></h6>
        </div>
            <button type="submit" class="btn btn-default" id="submit" name="submit">Submit</button>
    </form>        
    </div>

</body>