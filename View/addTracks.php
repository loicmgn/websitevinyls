<head>
    <title>Add tracks</title>

    <!-- Bootstrap core CSS -->
    <link href="../View/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../View/css/4-col-portfolio.css" rel="stylesheet">

    <!-- Add icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>

</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">         
            <a class="navbar-brand" href="../View/vinyls.php">Vinyzi</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="../View/vinyls.php">My Vinyls
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="../View/addVinyl.php">+ Add a Vinyl</a>
                        <span class="sr-only">(current)</span>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Controller/logoutController.php">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4"></div>
        <div class="col-sm-4"></div>
    </div>
    <div class="container" id="divAddVinyls">
        <div class="row" id="divTitle">
            <div class="col-sm" id="colTitle">
                <h3>Add tracks to your vinyl</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <form action="../Controller/addTracksController.php" method="POST">

                    <style>
                        /* #newlink {width:600px} */
                        /* http://www.satya-weblog.com/2010/02/add-input-fields-dynamically-to-form-using-javascript.html */
                    </style>
                    <div id="newTrack">
                        <div>                                
                            <table border=0>
                                <tr>
                                    <td>
                                        <h4>Track N.1</h4>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td> Track Title : </td>
                                    <td> <input class="form-control" type="text" name="trackTitle[]"> </td>
                                </tr>
                                <tr>
                                    <td> Artist : </td>
                                    <td> <input class="form-control" type="text" name="trackArtist[]"> </td>
                                </tr>
                                <tr>
                                    <td> Style : </td>
                                    <td> <input class="form-control" type="text" name="trackStyle[]"> </td>
                                </tr>     
                                <tr>
                                    <td> Duration : </td>
                                    <td> <input class="form-control" type="text" name="trackDuration[]"> </td>
                                </tr>
                                <tr>
                                    <td> BPM : </td>
                                    <td> <input class="form-control" type="text" name="trackBpm[]"> </td>
                                </tr>
                                <tr>
                                    <td> Notation: </td>
                                    <td> <input class="form-control" type="number" min="1" max="5" name="trackNotation[]"> </td>
                                </tr>
                                <tr>
                                    <td> Comment : </td>
                                    <td> <input class="form-control" type="text" name="trackComment[]"> </td>
                                </tr>  
                            </table>
                        </div>
                    </div>
                    <p id="addnew">
                        <a href="#" id="addNew">Add New </a>
                    </p>
                    <!-- Template -->
                    <div id="newTrackTpl" style="display:none">
                        <div>                                
                            <table border=0>
                                <tr>
                                    <td>
                                        <h4>Track N.<span class="number"></span></h4>
                                    </td>
                                </tr> 
                                <tr>
                                    <td> Track Title : </td>
                                    <td> <input class="form-control" type="text" name="trackTitle[]"> </td>
                                </tr>
                                <tr>
                                    <td> Artist : </td>
                                    <td> <input class="form-control" type="text" name="trackArtist[]"> </td>
                                </tr>
                                <tr>
                                    <td> Style : </td>
                                    <td> <input class="form-control" type="text" name="trackStyle[]"> </td>
                                </tr>     
                                <tr>
                                    <td> Duration : </td>
                                    <td> <input class="form-control" type="text" name="trackDuration[]"> </td>
                                </tr>
                                <tr>
                                    <td> BPM : </td>
                                    <td> <input class="form-control" type="text" name="trackBpm[]"> </td>
                                </tr>
                                <tr>
                                    <td> Notation : </td>
                                    <td> <input class="form-control" type="number" min="1" max="5" name="trackNotation[]"> </td>
                                </tr>
                                <tr>
                                    <td> Comment : </td>
                                    <td> <input class="form-control" type="text" name="trackComment[]"> </td>
                                </tr>   
                            </table>
                        </div>
                    </div>
                    <center><button type="submit" class="btn btn-default" id="submitTracks" name="submitTracks">Submit</button></center>
                </form>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.3.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#addNew").click(new_link);
    });
    var ct = 1;
    function new_link()
    {
        ct++;
        let div1 = document.createElement('div');
        div1.id = ct;
        $(".number").text(ct);
        
        // link to delete extended form elements
        var delLink = '<div style="text-align:right;margin-right:65px"><a href="javascript:delIt(' + ct + ')">Del</a></div>';
        div1.innerHTML = document.getElementById('newTrackTpl').innerHTML + delLink;
        
        document.getElementById('newTrack').appendChild(div1);
    }
// function to delete the newly added set of elements
    function delIt(eleId)
    {
        d = document;
        var ele = d.getElementById(eleId);
        var parentEle = d.getElementById('newTrack');
        parentEle.removeChild(ele);
    }

</script>